import unittest

class TestApp(unittest.TestCase):

  def test_sum(self):
    import app
    self.assertEqual(app.sum(2,3),5)
    
  def test_sub(self):
    import app
    self.assertEqual(app.sub(3,2),1)

if __name__ == "__main__":
  unittest.main()
